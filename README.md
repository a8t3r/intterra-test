### Intro
The first input line should contain the number of input lines (i.e., n)

###Running

```
git clone https://bitbucket.org/a8t3r/intterra-test
cd intterra-test
cat | ./gradlew -q --console=plain run
```

### Options

read input from file:

`./gradlew -q --console=plain run --args="input.txt"`

read input from stdin:

`cat input.txt | ./gradlew -q --console=plain run`

### Example
```
echo "5
user1 -> xxx@ya.ru, foo@gmail.com, lol@mail.ru
user2 -> foo@gmail.com, ups@pisem.net
user3 -> xyz@pisem.net, vasya@pupkin.com
user4 -> ups@pisem.net, aaa@bbb.ru
user5 -> xyz@pisem.net" | ./gradlew -q --console=plain run
```