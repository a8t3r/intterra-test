package ru.intterra.test;

import org.junit.Test;
import ru.intterra.test.model.User;
import ru.intterra.test.service.InputParser;
import ru.intterra.test.service.UserUniquenessService;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static com.google.common.io.Resources.getResource;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class UserUniquenessServiceTest {

    private final UserUniquenessService service = new UserUniquenessService();

    @Test
    public void should_find_non_unique() {
        Collection<User> unique = service.findUnique(asList(
                User.of("a").appendEmails("first@email"),
                User.of("b").appendEmails("second@email"),
                User.of("c").appendEmails("third@email")
        ));

        assertThat(unique)
                .hasSize(3)
                .contains(
                        User.of("a").appendEmails("first@email"),
                        User.of("b").appendEmails("second@email"),
                        User.of("c").appendEmails("third@email")
                );
    }

    @Test
    public void should_find_unique_from_file() throws IOException {
        List<User> users = new InputParser().parse(getResource("test_input_1.txt").openStream());
        Collection<User> unique = service.findUnique(users);

        assertThat(unique)
                .hasSize(2)
                .contains(User.of("user1").appendEmails("xxx@ya.ru", "foo@gmail.com", "lol@mail.ru", "ups@pisem.net", "aaa@bbb.ru"))
                .contains(User.of("user3").appendEmails("xyz@pisem.net", "vasya@pupkin.com"));
    }

    @Test
    public void should_find_unique_on_cycle() {
        Collection<User> unique = service.findUnique(asList(
                User.of("a").appendEmails("first@email"),
                User.of("b").appendEmails("second@email", "first@email"),
                User.of("c").appendEmails("second@email")
        ));

        assertThat(unique).containsOnly(User.of("a").appendEmails("second@email", "first@email"));
    }

    @Test
    public void should_find_unique_on_flatten() {
        Collection<User> unique = service.findUnique(asList(
                User.of("a").appendEmails("first@email"),
                User.of("b").appendEmails("second@email"),
                User.of("c").appendEmails("third@email"),
                User.of("d").appendEmails("first@email", "second@email", "third@email"))
        );

        assertThat(unique).containsOnly(User.of("a").appendEmails("first@email", "second@email", "third@email"));
    }
}