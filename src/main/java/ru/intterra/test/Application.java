package ru.intterra.test;

import ru.intterra.test.model.User;
import ru.intterra.test.service.InputParser;
import ru.intterra.test.service.UserUniquenessService;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

public class Application {

    public static void main(String[] args) throws IOException {
        InputStream inputStream = args.length > 0 ? new FileInputStream(args[0]) : System.in;
        List<User> users = new InputParser().parse(inputStream);
        Collection<User> unique = new UserUniquenessService().findUnique(users);
        unique.forEach(System.out::println);
    }
}
