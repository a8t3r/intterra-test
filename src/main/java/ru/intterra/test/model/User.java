package ru.intterra.test.model;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

@Value
@EqualsAndHashCode
public class User {

    private final String username;
    private final Set<String> emails = new LinkedHashSet<>();

    public User appendEmails(String... emails) {
        return appendEmails(Arrays.asList(emails));
    }

    public User appendEmails(Collection<String> emails) {
        this.emails.addAll(emails);
        return this;
    }

    @Override
    public String toString() {
        return username + " -> " + StringUtils.join(emails, ", ");
    }

    public static User of(String username) {
        return new User(username);
    }
}
