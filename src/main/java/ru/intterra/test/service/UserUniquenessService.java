package ru.intterra.test.service;

import com.google.common.collect.Maps;
import ru.intterra.test.model.User;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserUniquenessService {

    public Collection<User> findUnique(Collection<User> users) {
        Map<String, User> emailToUserIndex = new HashMap<>();

        for (User user : users) {
            User uniqueUser = findAppropriateUniqueUser(emailToUserIndex, user)
                    .map(targetUser -> {
                        Set<String> collectedEmails = collectEmailsFromAliases(emailToUserIndex, user);
                        targetUser.appendEmails(collectedEmails);
                        targetUser.appendEmails(user.getEmails());
                        return targetUser;
                    })
                    .orElse(user);

            // update index with corresponding keys
            emailToUserIndex.putAll(Maps.asMap(uniqueUser.getEmails(), x -> uniqueUser));
        }

        return new LinkedHashSet<>(emailToUserIndex.values());
    }

    private Set<String> collectEmailsFromAliases(Map<String, User> emailToUserIndex, User user) {
        return findAliases(emailToUserIndex, user)
                .flatMap(it -> it.getEmails().stream())
                .collect(Collectors.toSet());
    }

    private Optional<User> findAppropriateUniqueUser(Map<String, User> emailToUserIndex, User user) {
        return findAliases(emailToUserIndex, user)
                .findFirst();
    }

    private Stream<User> findAliases(Map<String, User> emailToUserIndex, User user) {
        return user.getEmails().stream()
                .map(emailToUserIndex::get)
                .filter(Objects::nonNull);
    }
}
