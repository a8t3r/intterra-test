package ru.intterra.test.service;

import ru.intterra.test.model.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.splitByWholeSeparator;

public class InputParser {

    public List<User> parse(InputStream inputStream) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            int lines = Integer.parseInt(reader.readLine());

            return reader.lines()
                    .limit(lines)
                    .map(this::parseUser)
                    .collect(Collectors.toList());
        }
    }

    private User parseUser(String line) {
        String[] parts = splitByWholeSeparator(line, " -> ");
        return User.of(parts[0])
                .appendEmails(splitByWholeSeparator(parts[1], ", "));
    }
}
